---
title: Dateien
image: data.png
categories:
  - Voraussetzung
description:
  Einführung in den Inhalt der für diesen Kurs verteilten Datensätze.
draft: false
---

# Dateien Herunterladen

Die benötigte Dateien für diesen Kurs stehen zur Verfügung unter folgenden [Link](Ressourcen/Dateien/KursDateien.zip).

Um diese Dateien direkt in deinem Arbeitsverzeichnis zu speichern, führe
folgende Befehle in deiner Sitzung:

<!-- Die Dateien, die für diesen Kurs benötigt werden, sind [im folgenden Repo](https://gitlab.com/kamapu/courses/rstats/blob/main/Lernmaterial/Dateien/data/) hochgeladen.
Alle Dateien können während der laufenden Sitzung mit den folgenden Befehlen heruntergeladen werden.
Um diese Befehle auszuführen, muss [Git](https://git-scm.com/downloads) auf deinem System installiert sein. -->


```{r}
#| eval: false
download.file(url = "https://rstats.kamapu.net/Ressourcen/Dateien/KursDateien.zip",
    destfile = "KursDateien.zip")
unzip("KursDateien.zip", overwrite = TRUE)
unlink("KursDateien.zip")
```


```{r}
#| eval: false
#| echo: false
repo_url <- "https://gitlab.com/kamapu/courses/rstats.git"
output_dir <- "data"
temp_dir <- "temp"
folder_path <- "Ressourcen/Dateien/data"
dir.create(output_dir)
dir.create(temp_dir)
system(paste("git clone --depth 1 --no-checkout", repo_url, temp_dir))
setwd(temp_dir)
system("git config core.sparseCheckout true")
write(folder_path, file = ".git/info/sparse-checkout")
system("git checkout")
setwd("../")
files <- list.files(file.path(temp_dir, folder_path), full.names = TRUE)
file.copy(files, output_dir, overwrite = TRUE)
unlink(temp_dir, recursive = TRUE)
```


<!-- # Beschreibung der Datensätzen

Hier die Beschreibung demnächst... -->
