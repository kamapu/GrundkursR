---
title: "see-preamble"
subtitle: "[Termin 3]"
author: "see-preamble"
date: "05. November 2024"
output:
  beamer_presentation:
    theme: Berkeley
    fonttheme: structurebold
    slide_level: 2
header-includes: |
  ```{=latex}
  % Display margins in document
  %\usepackage{showframe}
  
  % No section slides
  \AtBeginSection[]{}
  
  % Add a logo
  \pgfdeclareimage[height=1.5cm]{logo}{images/logo-vhs.png}
  \logo{\pgfuseimage{logo}}
  
  % Customized URL and hyperlinks
  \newcommand{\urlm}[2][blue]{\textcolor{#1}{\url{#2}}}
  \newcommand{\hrefm}[3][blue]{\textcolor{#1}{\href{#2}{#3}}}
  
  % Enable overlayed text boxes
  \usepackage[overlay]{textpos}
  
  % Optimize columns call ------------------------------------------------------
  \def\begincols{\begin{columns}}
  \def\begincol{\begin{column}}
  \def\endcol{\end{column}}
  \def\endcols{\end{columns}}
  
  % Set colors -----------------------------------------------------------------
  
  % Define Kenian colors
  \definecolor{vhsorange}{HTML}{eb640f}
  \definecolor{vhsyellow}{HTML}{fab90f}
  \definecolor{vhsdarkblue}{HTML}{00285a}
  \definecolor{vhslightblue}{HTML}{64b9e6}
  \definecolor{vhsblue}{HTML}{1e73af}
  \definecolor{vhsred}{HTML}{e1000f}
  \definecolor{mygrey}{RGB}{155,155,155}
  
  % Set palette
  
  % Title box at title slide
  \setbeamercolor{palette primary}{fg=white,bg=vhsred}
  % Logo box at the top-left corner
  \setbeamercolor{palette secondary}{fg=white,bg=white}
  % Top bar with section/sub-section title
  \setbeamercolor{frametitle}{fg=white,bg=vhsblue}
  % Active section in navigation bar
  \setbeamercolor{palette sidebar secondary}{fg=vhsred,bg=white}
  \setbeamercolor{section in sidebar shaded}{fg=mygrey}
  % Title color in sidebar
  \setbeamercolor{title in sidebar}{fg=white}
  % Authors name color in sidebar
  \setbeamercolor{author in sidebar}{fg=vhsyellow}
  % Section color in sidebar
  %\setbeamercolor{palette sidebar secondary}{fg=yellow}
  
  % Layout for sidebar
  \makeatletter
  \setbeamertemplate{sidebar canvas left}[vertical shading][
  top=vhsdarkblue,bottom=vhsdarkblue]
  \makeatother
  
  % Numbering slides
  \setbeamertemplate{footline}[frame number]
  
  % Itemized lists -------------------------------------------------------------
  \setbeamertemplate{itemize item}[triangle]
  %\setbeamercolor{itemize item}{fg=vhsorange}
  \setbeamercolor{itemize item}{fg=vhslightblue}

  \setbeamertemplate{itemize subitem}[triangle]
  %\setbeamercolor{itemize subitem}{fg=vhslightblue}
  \setbeamercolor{itemize subitem}{fg=vhsorange}
  
  \setlength{\leftmargini}{0.5cm}
  \setlength{\leftmarginii}{0.3cm}
  
  % Title and author -----------------------------------------------------------
  
  % Customized long and short author
  \AtBeginDocument{\author[M Alvarez]{Miguel Alvarez}}
  
  % Customized long and short title
  \AtBeginDocument{\title[R Stats]{%
  Statistik und Graphiken mit R}}
  
  % New commands ---------------------------------------------------------------
  \newcommand{\myurl}[1]{{\color{blue} \url{#1}}}
  ```
classoption:
  - aspectratio=169
  - t
---

```{r preamble, echo = FALSE, message = FALSE}
library(knitr)

# Set chunk options
opts_chunk$set(
    out.width = "\\textwidth",
    fig.align = "center",
    fig.asp = 0.94,
    fig.width = 6)
```


# Der Kurs


\begincols \begincol{0.5\textwidth}

- **Termin 1 & 2**
  - Grundlagen
  - Datentypen

\vspace{0.5cm}

- **Termin 3 & 4**
  - Objekten
  - Lesen und Schreiben

\endcol \begincol{0.5\textwidth}

- **Termin 5 & 6**
  - Statistiken
  - Graphiken (1)

\vspace{0.5cm}

- **Termin 7 & 8**
  - Graphiken (2)
  - Fortgeschrittenes Programmieren
  - Abschluss

\endcol \endcols


# Experiment

\begincols \begincol{0.5\textwidth}

![](images/jelly-beans.jpg)

\endcol \begincol{0.5\textwidth}

Wie viele Jelly Beans sind in diesem Behälter?

\endcol \endcols


# Pakete

\begincols \begincol{0.5\textwidth}

**CRAN**\
(Comprehensive R Archive Network)

\vspace{0.3cm}

- `install.packages()`
- `update.packages()`

\vspace{0.5cm}

\scriptsize

```{r eval=FALSE}
install.packages("ade4")
update.packages(ask = FALSE)
```

\vspace{0.3cm}

\myurl{https://cran.r-project.org/}

\endcol \begincol{0.5\textwidth}

```{r echo=FALSE}
include_graphics("images/cran.png")
```

\endcol \endcols


## Pakete

\begincols \begincol{0.5\textwidth}

**devtools**

\vspace{0.3cm}

- `install()`
- `install_github()`

\vspace{1cm}

\scriptsize

\myurl{https://ropensci.org/}

\endcol \begincol{0.5\textwidth}

```{r echo=FALSE}
include_graphics("images/devtools.png")
```

\endcol \endcols


# Import/Export

\begincols \begincol{0.4\textwidth}

- `readLines()`
- `read.table()`
  - `read.csv()`
  - `read.csv2()`

\endcol \begincol{0.6\textwidth}

\scriptsize

```{r}
Bonn2021 <- read.csv("Bevoelkerung-2021.csv")
str(Bonn2021)
```

\endcol \endcols

## Import/Export

\begincols \begincol{0.4\textwidth}

- `readLines()`
- `read.table()`
  - `read.csv()`
  - `read.csv2()`

\vspace{0.5cm}

- `write.table()`
  - `write.csv()`
  - `write.csv2()`

\endcol \begincol{0.6\textwidth}

\scriptsize

```{r eval=FALSE}
write.csv(iris, file = "iris.csv")
write.csv2(iris, file = "iris2.csv")
```

\endcol \endcols


## Import/Export

\begincols \begincol{0.5\textwidth}

Pakete können eigene Funktionen für Importieren und Exportieren anbieten.

\vspace{0.5cm}

- **xlsx**
  - `read.xlsx()`
  - `write.xlsx()`

\vspace{0.5cm}

- **readODS**
  - `read_ods()`
  - `write_ods()`

\endcol \begincol{0.5\textwidth}

```{r echo=FALSE}
include_graphics("images/R-import-export.png")
```

\endcol \endcols


## Import/Export

\begincols \begincol{0.5\textwidth}

Pakete können eigene Funktionen für Importieren und Exportieren anbieten.

\vspace{0.5cm}

- **xlsx**
  - `read.xlsx()`
  - `write.xlsx()`

\vspace{0.5cm}

- **readODS**
  - `read_ods()`
  - `write_ods()`

\endcol \begincol{0.5\textwidth}

```{r echo=FALSE}
include_graphics("images/R-import-export.png")
```

\endcol \endcols


## Import/Export

\begincols \begincol{0.5\textwidth}

**R-Images**

\vspace{0.3cm}

- Workspace
  - `save()`
  - `load()`
  - Dateierweiterung **.rda** oder **.RData**

\vspace{0.3cm}

- Einzelnes Objekt
  - `saveRDS()`
  - `readRDS()`
  - Dateierweiterung **.rds**

\endcol \begincol{0.5\textwidth}

\endcol \endcols

#

\LARGE

**Vielen Dank!**

\vspace{1cm}

\scriptsize

```{r}
library(fortunes)
fortune(10)
```

