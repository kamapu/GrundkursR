---
title: Einführung in R und RStudio
subtitle: "[Termin 4]"
author: Miguel Alvarez
date: 2024-11-07
format:
  kamapu-beamer:
    titleimage: images/flying-documents-cover.jpg
    titlecol: ff0919
execute:
  echo: true
---

# Der Kurs

:::: {.columns}
::: {.column}

- ~~**Termin 1**~~
  - ~~Grundlagen von R~~
  - ~~Einführung in RStudio~~
- ~~**Termin 2**~~
  - ~~Vektorenklassen~~
  - ~~Objekten~~
- ~~**Termin 3**~~
  - ~~Daten Lesen und Schreiben~~
- ~~**Termin 4**~~
  - ~~Explorative Datenanalyse (EDA)~~
  - ~~Datenvisualisierung~~

:::
::: {.column}

- ~~**Termin 5**~~
  - ~~Syntax in `tidyverse`~~
  - ~~Grafiken mit `ggplot2`~~
- **Termin 6**
  - Funktionen selber definieren
  - *To loop or not to loop*
- **Termin 7**
  - Jocker-Karte
- **Termin 8**
  - Schluss

:::
::::

# Funktionen Definieren

Beispiele über Funktionen Definieren

Funktionen in Funktionen


# To loop or not to loop

Beispiele von Loops (Schleifen)

# `source()` Skripten

Skripten mit `source()` durchführen


# Vielen Dank!

\vspace{1cm}

\scriptsize

```{r}
library(fortunes)
fortune(43)
```
