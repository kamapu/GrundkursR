# Set working directory
wd <- "Ressourcen/Folien"

# Install Extension
setwd(wd)
system("quarto add --no-prompt https://gitlab.com/quarto-extensions/kamapu/-/archive/main/shorty-main.zip")
setwd("../../")

# Copy Data Sets
file.copy(from = file.path("Ressourcen/Dateien/data",
        c("Bevoelkerung-2021.csv",
          "BonnBevoelkerung.rds",
          "BonnBezirke.rds")), to = wd, overwrite = TRUE)

# Scan quarto files to trim images

